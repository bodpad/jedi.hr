from django.db import models


class Candidate(models.Model):
    """
    Кандидаты в падаваны
    """
    name = models.CharField(max_length=255, verbose_name='Имя', unique=True)
    age = models.PositiveSmallIntegerField(verbose_name='Возраст')
    email = models.EmailField(unique=True)
    enrolled = models.BooleanField(default=False)  # Зачислен в падаваны или нет
    passed_the_test = models.BooleanField(default=False)  # прошел ли кандидат тестирование или нет
    planet = models.ForeignKey('Planet', on_delete=models.CASCADE, verbose_name='Планета обитания')

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('candidate_detail', args=[str(self.id)])


class Jedi(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя Джедая')
    planet = models.ForeignKey('Planet', on_delete=models.CASCADE, verbose_name='Планета на которой он обучает')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Джедая'
        verbose_name_plural = 'Джедаи'


class Planet(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название планеты')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Планету'
        verbose_name_plural = 'Планеты'


class Question(models.Model):
    """
    Список вопросов задаваемых кандидатам в Джедаи
    """
    question = models.CharField(max_length=255, verbose_name='Вопрос к кандидату в падаваны')

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы к кандидатам в падаваны'


class Answer(models.Model):
    """
    Ответы на вопросы задаваемые кандидатам в Джедаи
    """
    candidate = models.ForeignKey('Candidate', on_delete=models.CASCADE)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answer = models.BooleanField()

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы задаваемые кандидатам'