from django.http import HttpResponse, Http404, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods

from project.settings import EMAIL_HOST_USER
from .send_mail import send_mail
from .models import Candidate, Jedi, Answer
from .forms import CandidateForm, JediForm, QuestionForm


@require_http_methods(["GET"])
def homapage(request):
    return redirect('jedi')


@require_http_methods(["GET"])
def jedi(request):
    form = JediForm()
    return render(request, 'main/jedi.html', {'form': form})


require_http_methods(["GET", "POST"])
def apply_as_candidate_step1(request):

    """
    Регистрация кандидата в падаваны.
    Шаг1. Указание информации о себе
    """

    if request.method == 'GET':
        form = CandidateForm()
        return render(request, 'main/apply-as-candidate-step1.html', {'form': form})

    else:
        form = CandidateForm(request.POST)

        if form.is_valid():
            candidate = form.save()
            request.session['user_id'] = candidate.id
            return redirect('apply_as_candidate_step2')

        else:
            return render(request, 'main/apply-as-candidate-step1.html', {'form': form})


require_http_methods(["GET", "POST"])
def apply_as_candidate_step2(request):

    """
    Регистрация кандидата в падаваны. 
    Шаг2. Ответы на предложенные вопросы
    """

    if request.method == 'GET':

        form = QuestionForm()

        context = {
            'candidate_id': request.session['user_id'],
            'form': form
        }

        return render(request, 'main/apply-as-candidate-step2.html', context)

    else:

        form = QuestionForm(request.POST)

        if form.is_valid():

            candidate_id = request.session['user_id']

            try:
                candidate = Candidate.objects.get(id=candidate_id)
            except:
                return HttpResponseBadRequest()

            for field_name, answer in form.cleaned_data.items():

                # Из-за того, что вопросы для тестирования кандидата берутся из бд(модель "Question"),
                # все поля формы "QuestionForm" инициализируюся перед созданием экземляра формы.
                # Поэтому было принято решение давать названия динамическим полям офрмы
                # в таком формате: question_n, где n - это идентификатор вопроса.
                # Поэтому для получения id вопроса необхоимо извлечь срез field_name[9:]

                question_id = field_name[9:]

                # Циклично заносим ответы кандидата в БД
                Answer.objects.create(candidate=candidate, question_id=question_id, answer=answer)

            # Указываем, что кандидат прошел тестирование
            candidate.passed_the_test = True
            candidate.save()

            return redirect('apply_as_candidate_done')

        else:
            return render(request, 'main/apply-as-candidate-step2.html', {'form': form})


@require_http_methods(["GET"])
def apply_as_candidate_done(request):

    """
    Завершающая страница регистрации в кандидаты
    """

    if request.session.has_key('user_id'):
        del request.session['user_id']
        return render(request, 'main/apply-as-candidate-done.html')

    else:
        return redirect('apply_as_candidate_step1')


@require_http_methods(["GET"])
def candidate_master(request):

    """
    Список кандидатов выбранного Джедая.
    Имя Джедая передается GET параметром и является обязательным.
    """

    try:
        jedi_name = request.GET['jedi']
        jedi = Jedi.objects.get(name=jedi_name)
    except:
        return redirect('jedi')

    # Извлекаем всех кандидатов выбранного Джедая прошедших тетсирование,
    # но еще не зачисленных в падаваны
    candidates = Candidate.objects.filter(planet=jedi.planet, enrolled=False, passed_the_test=True)

    context = {
        'jedi': jedi,
        'candidates': candidates
    }

    return render(request, 'main/candidate-master.html', context)


@require_http_methods(["GET"])
def candidate_detail(request, id):

    """
    Детальная информация по выбранному кандидату
    """

    try:
        candidate = Candidate.objects.get(id=id)
        answers = candidate.answer_set.filter(candidate=candidate)

        context = {
            'candidate': candidate,
            'answers': answers
        }

        return render(request, 'main/candidate-detail.html', context)

    except Candidate.DoesNotExist:
        raise Http404("Кандидат не найден")


@require_http_methods(["POST"])
def enroll_in_padawans(request):

    """
    Записываем кандидата в падаваны и оповещаем его по email
    """

    try:
        cid = request.POST['cid']  # candidate id
        candidate = Candidate.objects.get(id=cid)
        candidate.enrolled = True
        candidate.save()

        send_mail(
            'Поздравляем!!!',
            'Вы зачислены в Падаваны.',
            EMAIL_HOST_USER,
            [candidate.email],
            fail_silently=False,
        )

        return redirect(candidate)

    except:
        return HttpResponseBadRequest()