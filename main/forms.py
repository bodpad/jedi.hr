from django import forms

from .models import Candidate, Jedi, Question


class JediForm(forms.Form):
    jedi_list = [(jedi.name, jedi.name) for jedi in Jedi.objects.all()]
    jedi = forms.ChoiceField(choices=jedi_list, label="Имя Джедая", widget=forms.Select(attrs={'class': 'form-control'}))


class CandidateForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = ['name', 'email', 'age', 'planet']

    def __init__(self, *args, **kwargs):
        super(CandidateForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class']   = 'form-control'
        self.fields['email'].widget.attrs['class']  = 'form-control'
        self.fields['age'].widget.attrs['class']    = 'form-control'
        self.fields['planet'].widget.attrs['class'] = 'form-control'


class QuestionForm(forms.Form):
    """
    Форма с динамическим кол-вом полей.
    Поля формы берутся из модели Question
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        questions = Question.objects.all()
        for question in questions:
            self.fields.update({
                'question_%s' % question.id: forms.BooleanField(label=question.question, required=False)
            })

    def save(self, commit=True):
        self.cleaned_data = dict([(k, v) for k, v in self.cleaned_data.items() if v != ""])
        return super(QuestionForm, self).save(commit=commit)