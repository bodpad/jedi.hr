from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.homapage, name='homapage'),
    url(r'^jedi/$', views.jedi, name='jedi'),
    url(r'^candidates/$', views.candidate_master, name='candidate_master'),
    url(r'^candidates/(\d+)/$', views.candidate_detail, name='candidate_detail'),
    url(r'^apply_as_candidate/step1/$', views.apply_as_candidate_step1, name='apply_as_candidate_step1'),
    url(r'^apply_as_candidate/step2/$', views.apply_as_candidate_step2, name='apply_as_candidate_step2'),
    url(r'^apply_as_candidate/done/$', views.apply_as_candidate_done, name='apply_as_candidate_done'),
    url(r'^enroll_in_padawans/$', views.enroll_in_padawans, name='enroll_in_padawans'),
]
