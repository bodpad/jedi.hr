from django.contrib import admin

from .models import Jedi, Planet, Question


@admin.register(Jedi)
class JediAdmin(admin.ModelAdmin):
    list_display = ('name', 'planet')


@admin.register(Planet)
class PlanetAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    #list_display = ('name', 'country')
    pass
